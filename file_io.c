#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include "file_io.h"
#include "request.h"

char* www_folder = NULL;

void file_io_initialize(char* folder)
{
    DIR* test_dir;

    if (www_folder == NULL)
        www_folder = (char *)malloc(strlen(folder)+1);
    if (www_folder == NULL)
    {
        fprintf(stderr, "No memory to record www folder path");
        exit(EXIT_FAILURE);
    }
    strcpy(www_folder, folder);
    test_dir = opendir(www_folder);
    if (test_dir)
    {
        closedir(test_dir);
    }
    else
    {
        fprintf(stdout, "Can't open www folder: %s\n", www_folder);
        exit(EXIT_FAILURE);
    }
}

void parse_node_uri(request_node* node)
{
    char* path;
    char* query;
    char* rear_slash;
    char* parent_slash;
    char* colon_slash;
    int path_len;
    char parsed_uri[MAX_PATH_LENGTH];

    if ((colon_slash = strstr(node->uri, "://")) != NULL)
        path = strchr(colon_slash+3, '/');
    else
        path = strchr(node->uri, '/');
    if (path == NULL)
        query = NULL;
    else
    {
        query = strchr(path, '?');
        if (query)
        {
            *query = '\0';
            query++;
        }
    }
    if (path == NULL)
        strcpy(parsed_uri, "/index.html");
    else
    {
        while ((parent_slash = strstr(path, "/../")) != NULL)
        {
            parent_slash[1] = '/';
            parent_slash[2] = '/';
            while (parent_slash != path &&
                   *(parent_slash - 1) != '/')
            {
                parent_slash--;
                *parent_slash = '/';
            }
        }
        path_len = strlen(path);
        if (path_len >= 3 && strcmp(path+path_len-3, "/..") == 0)
        {
            parent_slash = path+path_len-3;
            parent_slash[1] = '/';
            parent_slash[2] = '/';
            while (parent_slash != path &&
                   *(parent_slash - 1) != '/')
            {
                parent_slash--;
                *parent_slash = '/';
            }
        }
        rear_slash = path;
        while (*rear_slash == '/')
            rear_slash++;
        if (*rear_slash == '\0')
            strcpy(parsed_uri, "/index.html");
        else
            strcpy(parsed_uri, path);
    }
    strcpy(node->uri, parsed_uri);
    if (query)
        strcpy(node->query ,query);
}

void file_io_close()
{
    free(www_folder);
}

int get_file(request_node* node)
{
    char realpath[MAXLINE];

    strcpy(realpath, www_folder);
    strcat(realpath, node->uri);
    if (node->method == GET)
    {
        if ((node->file_fp = fopen(realpath, "rb")) == NULL)
            return 0;
        if (fstat(fileno(node->file_fp), &(node->file_meta)) == 0)
        {
            if (S_ISREG(node->file_meta.st_mode))
                return 1;
            else
                return 0;
        }
    }
    else if (node->method == POST)
    {
        if ((node->file_fp = fopen(realpath, "rb")) == NULL)
            return 0;
        if (fstat(fileno(node->file_fp), &(node->file_meta)) == 0)
        {
            if (S_ISREG(node->file_meta.st_mode))
                return 1;
            else
                return 0;
        }
    }
    else if (node->method == HEAD)
    {
        if (stat(realpath, &(node->file_meta)) == 0)
        {
            if (S_ISREG(node->file_meta.st_mode))
                return 1;
            else
                return 0;
        }
    }
    return 0;
}

int file_exist(char* path)
{
    struct stat buf;

    return (stat(path, &buf) == 0);
}
