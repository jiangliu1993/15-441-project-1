#ifndef REQUEST_H
#define REQUEST_H

#include <stdio.h>
#include <sys/stat.h>

#define MAXLINE 8192
#define MAX_HEADER_NUM 128
#define MAX_BODY_SIZE 1048576 // 2MB

typedef struct response_line response_line;
typedef struct connection connection;

typedef enum {GET, POST, HEAD, NONE} request_method;

typedef enum {
    START, HEADER, BODY, ABORT, DONE, CGI, READY
} request_stage;

typedef struct request_headers {
    char accept[MAXLINE];
    char accept_charset[MAXLINE];
    char accept_encoding[MAXLINE];
    char accept_language[MAXLINE];
    char content_type[MAXLINE];
    char cookie[MAXLINE];
    char host[MAXLINE];
    char referer[MAXLINE];
    char user_agent[MAXLINE];
} request_headers;

typedef struct request_node {
    /* Request parsing stage */
    request_stage stage;
    /* Request info */
    request_method method;
    char uri[MAXLINE];
    char query[MAXLINE];
    request_headers headers;
    char close;
    int header_num;
    int body_size;
    int body_read;
    int body_write;
    char* body_buf;
    struct request_node* next;
    /* Response to the request */
    int status_code;
    response_line* head;
    response_line* tail;
    int cgi_pipe_in;
    int cgi_pipe_out;
    char cgi_buf[MAXLINE];
    int cgi_buf_size;
    char cgi_finish;
    FILE* file_fp;
    int file_read;
    struct stat file_meta;
} request_node;

typedef struct request_queue {
    request_node* head;
    request_node* tail;
} request_queue;

request_node* create_request_node();
void request_node_free(request_node* node);

request_queue* create_request_queue();
void request_queue_add_node(request_queue* queue, request_node* node);
request_node* request_queue_pop(request_queue* queue);
void request_queue_free(request_queue* queue);

void handle_connection_recv(connection* conn);

void read_message_body(connection* conn);
int connection_readline(connection* conn, char* line_buf);

void parse_request_line(request_node* node, char* line_buf);
void parse_request_header(request_node* node, char* line_buf);

#endif
