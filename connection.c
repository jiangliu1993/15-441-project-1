#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include "connection.h"
#include "log.h"
#include "response.h"
#include "cgi.h"

int http_port = 0;
int https_port = 0;
running_state state;

/* Create a connection for a socket */
connection* create_http_connection(int conn_fd)
{
    connection* conn;

    conn = (connection *)malloc(sizeof(connection));
    if (conn == NULL)
        return NULL;
    conn->timestamp = (unsigned long)time(NULL);
    conn->conn_fd = conn_fd;
    conn->client_context = NULL;
    conn->buf_size = 0;
    conn->current_request = NULL;
    conn->queue = create_request_queue();
    if (conn->queue == NULL)
    {
        free(conn);
        return NULL;
    }
    conn->close = 0;
    conn->handshaking = 0;
    conn->next = NULL; // Missed a line here, 2hrs of debugging

    return conn;
}

connection* create_https_connection(int conn_fd, SSL* context)
{
    connection* conn;

    conn = (connection *)malloc(sizeof(connection));
    if (conn == NULL)
        return NULL;
    conn->timestamp = (unsigned long)time(NULL);
    conn->conn_fd = conn_fd;
    conn->client_context = context;
    conn->buf_size = 0;
    conn->current_request = NULL;
    conn->queue = create_request_queue();
    if (conn->queue == NULL)
    {
        free(conn);
        return NULL;
    }
    conn->close = 0;
    conn->next = NULL;

    return conn;
}

/* Free a connection */
void connection_free(connection* conn)
{
    if (conn->client_context != NULL)
    {
        SSL_shutdown(conn->client_context);
        SSL_free(conn->client_context);
    }
    close(conn->conn_fd);
    if (conn->current_request != NULL)
        request_node_free(conn->current_request);
    if (conn->queue != NULL)
        request_queue_free(conn->queue);
    free(conn);
}

/* Create a connection list */
conn_list* create_connection_list()
{
    conn_list* list;

    list = (conn_list *)malloc(sizeof(conn_list));
    if (list == NULL)
        return NULL;
    list->size = 0;
    list->head = NULL;
    list->tail = NULL;

    return list;
}

/* Add a new connection to the rear of the list */
void connection_list_add(conn_list* list, connection* conn)
{
    if (list->head == NULL)
    {
        list->head = conn;
        list->tail = conn;
    }
    else
    {
        list->tail->next = conn;
        list->tail = conn;
    }
    list->size++;
}

/* Remove a connection from the list and free it */
void connection_list_remove(conn_list* list, int conn_fd)
{
    connection* conn;
    connection* temp;

    temp = NULL;
    conn = list->head;
    while (conn)
    {
        if (conn->conn_fd == conn_fd)
        {
            if (conn == list->head)
            {
                list->head = conn->next;
                if (conn == list->tail)
                    list->tail = NULL;
            }
            else if (conn == list->tail)
            {
                list->tail = temp;
                temp->next = NULL;
            }
            else
            {
                temp->next = conn->next;
            }
            connection_free(conn);
            list->size--;
            return;
        }
        temp = conn;
        conn = conn->next;
    }
}

/* Free the whole connection list */
void connection_list_free(conn_list* list)
{
    connection* conn;
    connection* temp;

    conn = list->head;
    while (conn)
    {
        temp = conn->next;
        connection_free(conn);
        conn = temp;
    }
    free(list);
}

/* Set a socket to the nonblock mode */
int nonblock(int sock)
{
    int flags;
    if ((flags = fcntl(sock, F_GETFL)) < 0)
    {
        log_log(sock, "Fail to get flags by fcntl");
        return 0;
    }
    if (fcntl(sock, F_SETFL, flags | O_NONBLOCK) < 0)
    {
        log_log(sock, "Fail to set flags by fcntl");
        return 0;
    }
    return 1;
}

/* Generate the fd_set for select from current connections */
int update_fdset(fd_set* readfds, fd_set* writefds, int listen_sock, int listen_s_sock, conn_list* list)
{
    int maxfd = 0;
    unsigned long current_time;
    connection* conn;
    connection* temp;

    FD_ZERO(readfds);
    FD_ZERO(writefds);
    FD_SET(listen_sock, readfds);
    FD_SET(listen_s_sock, readfds);
    if (listen_sock > listen_s_sock)
        maxfd = listen_sock;
    else
        maxfd = listen_s_sock;
    conn = list->head;
    current_time = (unsigned long)time(NULL);
    while (conn)
    {
        if (current_time - conn->timestamp >= TIME_OUT)
        {
            if ((conn->current_request == NULL ||
                 conn->current_request->stage == START) &&
                conn->queue->head == NULL)
            {
                log_log(conn->conn_fd, "Time out an idle socket");
                temp = conn->next;
                connection_list_remove(list, conn->conn_fd);
                conn = temp;
                continue;
            }
            else
            {
                log_log(conn->conn_fd, "Time out a request");
                if (conn->current_request)
                    request_node_free(conn->current_request);
                conn->current_request = NULL;
                if (conn->queue)
                    request_queue_free(conn->queue);
                conn->queue = create_request_queue();
                if (conn->queue == NULL)
                    conn->close = 1;
                else
                    generate_time_out_response(conn);
            }
        }
        if (conn->close)
        {
            log_log(conn->conn_fd, "Server close the socket");
            temp = conn->next;
            connection_list_remove(list, conn->conn_fd);
            conn = temp;
            continue;
        }
        FD_SET(conn->conn_fd, readfds);
        if (conn->queue->head != NULL)
        {
            if (conn->queue->head->stage == READY)
                FD_SET(conn->conn_fd, writefds);
            else if (conn->queue->head->stage == CGI)
            {
                if (conn->queue->head->body_size > 0 &&
                    (conn->queue->head->body_write <
                     conn->queue->head->body_size))
                {
                    FD_SET(conn->queue->head->cgi_pipe_in,
                            writefds);
                    if (conn->queue->head->cgi_pipe_in > maxfd)
                        maxfd = conn->queue->head->cgi_pipe_in;
                }
                if (!conn->queue->head->cgi_finish)
                {
                    FD_SET(conn->queue->head->cgi_pipe_out,
                            readfds);
                    if (conn->queue->head->cgi_pipe_out > maxfd)
                        maxfd = conn->queue->head->cgi_pipe_out;
                }
            }
        }
        if (conn->conn_fd > maxfd)
            maxfd = conn->conn_fd;
        conn = conn->next;
    }
    return maxfd;
}

/* Start to listen the given port */
int start_listen_sock(int port)
{
    int sock;
    struct sockaddr_in addr;

    /* Start listen socket */
    if ((sock = socket(PF_INET, SOCK_STREAM, 0)) == -1)
    {
        log_log(-1, "Fail to create socket");
        exit(EXIT_FAILURE);
    }
    if (!nonblock(sock))
    {
        log_log(sock, "Fail to set nonblock socket");
        exit(EXIT_FAILURE);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons((unsigned short)port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)))
    {
        log_log(sock, "Fail to bind socket");
        close(sock);
        exit(EXIT_FAILURE);
    }

    if (listen(sock, LISTEN_BACKLOG))
    {
        log_log(sock, "Fail to listen on socket");
        close(sock);
        exit(EXIT_FAILURE);
    }

    return sock;
}

SSL_CTX* create_ssl_context(char* key, char* cert)
{
    SSL_CTX* ssl_context;

    if ((ssl_context = SSL_CTX_new(TLSv1_server_method())) == NULL)
    {
        log_log(-1, "Error creating SSL context");
        exit(EXIT_FAILURE);
    }

    if (SSL_CTX_use_PrivateKey_file(ssl_context, key,
                SSL_FILETYPE_PEM) == 0)
    {
        SSL_CTX_free(ssl_context);
        log_log(-1, "Error associating private key");
        exit(EXIT_FAILURE);
    }

    if (SSL_CTX_use_certificate_file(ssl_context, cert,
                SSL_FILETYPE_PEM) == 0)
    {
        SSL_CTX_free(ssl_context);
        log_log(-1, "Error associating certificate");
        exit(EXIT_FAILURE);
    }

    return ssl_context;
}

SSL* accept_ssl_connection(int conn_fd, SSL_CTX* ssl_context)
{
    SSL* client_context;

    if ((client_context = SSL_new(ssl_context)) == NULL)
    {
        log_log(conn_fd, "Error creating client SSL context");
        return NULL;
    }

    if (SSL_set_fd(client_context, conn_fd) == 0)
    {
        log_log(conn_fd, "Error setting fd to client context");
        SSL_free(client_context);
        return NULL;
    }

    return client_context;
}

int http_recv(connection* conn)
{
    int n;
    n = recv(conn->conn_fd, conn->buf + conn->buf_size,
        DEFAULT_BUF_SIZE - conn->buf_size, 0);
    if (n == -1)
    {
        if (errno != EWOULDBLOCK && errno != EAGAIN)
        {
            log_log(conn->conn_fd, "Error on recv");
            return -1;
        }
        return 0;
    }
    if (n == 0)
    {
        log_log(conn->conn_fd, "Client closed the socket");
        return -1;
    }

    return n;
}

int https_recv(connection* conn)
{
    int n;
    int error;

    n = SSL_read(conn->client_context, conn->buf+conn->buf_size,
        DEFAULT_BUF_SIZE - conn->buf_size);
    if (n < 0)
    {
        error = SSL_get_error(conn->client_context, n);
        if (error != SSL_ERROR_WANT_READ &&
            error != SSL_ERROR_WANT_WRITE)
        {
            log_log(conn->conn_fd, "Error on https recv");
            return -1;
        }
        return 0;
    }
    if (n == 0)
    {
        log_log(conn->conn_fd, "Client closed the https socket");
        return -1;
    }

    return n;
}

void accept_http_connection(int sock, conn_list* connection_list)
{
    int client;
    struct sockaddr_in client_addr;
    socklen_t client_size;
    connection* conn;

    client_size = sizeof(client_addr);
    if ((client = accept(sock,
        (struct sockaddr *)&client_addr,&client_size))==-1)
    {
        if (errno != EWOULDBLOCK && errno != EAGAIN)
            log_log(-1, "Error on accept");
        return;
    }
    log_log(client, "Accept http connection");
    if (!nonblock(client))
    {
        log_log(client, "Fail to set nonblock socket");
        close(client);
        return;
    }
    conn = create_http_connection(client);
    if (conn == NULL)
    {
        log_log(client, "No memory to handle the new connection");
        close(client);
    }
    else
    {
        connection_list_add(connection_list, conn);
        if (connection_list->size > MAX_CONNECTIONS)
        {
            /* Too many connections now, slow down */
            generate_service_unavailable_response(conn);
        }
    }
}

void accept_https_connection(int sock, conn_list* connection_list,
        SSL_CTX* ssl_context)
{
    int client;
    struct sockaddr_in client_addr;
    socklen_t client_size;
    connection* conn;
    SSL* client_context;

    client_size = sizeof(client_addr);
    if ((client = accept(sock,
        (struct sockaddr *)&client_addr,&client_size))==-1)
    {
        if (errno != EWOULDBLOCK && errno != EAGAIN)
            log_log(-1, "Error on accept");
        return;
    }
    log_log(client, "Accept https connection");
    if (!nonblock(client))
    {
        log_log(client, "Fail to set nonblock socket");
        close(client);
        return;
    }
    client_context = accept_ssl_connection(client, ssl_context);
    if (client_context == NULL)
    {
        close(client);
    }
    else
    {
        conn = create_https_connection(client, client_context);
        if (conn == NULL)
        {
            log_log(client, "No memory to handle new connection");
            SSL_shutdown(client_context);
            SSL_free(client_context);
            close(client);
        }
        else
        {
            conn->handshaking = 1;
            /* Too many connections, slow down.
             * Also because the server is so occupied,
             * time consuming hankshake is unnecessary.
             * Thus service unavailable can't be sent. */
            if (connection_list->size >= MAX_CONNECTIONS)
                connection_free(conn);
            else
                connection_list_add(connection_list, conn);
        }
    }
}

/* Run the select loop to deal with connections */
running_state run_connections(int port, int s_port, char* key,
        char* cert)
{
    int sock, s_sock;
    fd_set readfds, writefds;
    int maxfd, readynum, n, error, testi;
    conn_list* connection_list;
    connection* conn;
    connection* temp;
    request_node* node;
    struct timeval tv;
    SSL_CTX* ssl_context;

    state = RUN;
    http_port = port;
    https_port = s_port;
    sock = start_listen_sock(port);    
    s_sock = start_listen_sock(s_port);
    connection_list = create_connection_list();
    if (connection_list == NULL)
    {
        log_log(-1, "No memory to create connection list");
        exit(EXIT_FAILURE);
    }
    ssl_context = create_ssl_context(key, cert);
    /* Begin the main loop waiting for things to do */
    while (state == RUN)
    {
        maxfd = update_fdset(&readfds, &writefds,
            sock, s_sock, connection_list);
        tv.tv_sec = TIME_OUT/2;
        tv.tv_usec = 0;
        if ((readynum =
            select(maxfd + 1, &readfds, &writefds, NULL, &tv)) < 0)
        {
            log_log(-1, "Error on select");
            continue;
        }
        /* Accept new http connections */
        if (readynum > 0 && FD_ISSET(sock, &readfds))
        {
            readynum--;
            accept_http_connection(sock, connection_list);
        }
        /* Accept new https connections */
        if (readynum > 0 && FD_ISSET(s_sock, &readfds))
        {
            readynum--;
            accept_https_connection(s_sock, connection_list,
                    ssl_context);
        }
        /* Traverse current connection list,
         * see if any connection needs to be deal with */
        conn = connection_list->head;
        while (conn && readynum > 0)
        {
            if (conn->queue->head &&
                conn->queue->head->stage == CGI)
            {
                if (FD_ISSET(conn->queue->head->cgi_pipe_in,
                            &writefds))
                {
                    readynum--;
                    conn->timestamp = (unsigned long)time(NULL);
                    handle_cgi_in(conn);
                    if (conn->close)
                    {
                        log_log(conn->conn_fd,
                            "Server closed the socket");
                        temp = conn->next;
                        connection_list_remove(connection_list,
                            conn->conn_fd);
                        conn = temp;
                        continue;
                    }
                }
                if (FD_ISSET(conn->queue->head->cgi_pipe_out,
                            &readfds))
                {
                    readynum--;
                    conn->timestamp = (unsigned long)time(NULL);
                    handle_cgi_out(conn);
                }
                if (conn->queue->head->cgi_finish &&
                    conn->queue->head->cgi_buf_size == 0)
                {
                    if (conn->close)
                    {
                        log_log(conn->conn_fd,
                            "Server closed the socket");
                        temp = conn->next;
                        connection_list_remove(connection_list,
                            conn->conn_fd);
                        conn = temp;
                        continue;
                    }
                    node = request_queue_pop(conn->queue);
                    request_node_free(node);
                }
                else if (conn->queue->head->cgi_buf_size > 0)
                {
                    if (forward_cgi_out(conn) == 0)
                    {
                        log_log(conn->conn_fd,
                            "Server closed the socket");
                        temp = conn->next;
                        connection_list_remove(connection_list,
                            conn->conn_fd);
                        conn = temp;
                        continue;
                    }
                    else if (conn->queue->head->cgi_finish &&
                             conn->queue->head->cgi_buf_size == 0)
                    {
                        if (conn->close)
                        {
                            log_log(conn->conn_fd,
                                "Server closed the socket");
                            temp = conn->next;
                            connection_list_remove(connection_list,
                                conn->conn_fd);
                            conn = temp;
                            continue;
                        }
                        node = request_queue_pop(conn->queue);
                        request_node_free(node);
                    }
                }
            }
            if (FD_ISSET(conn->conn_fd, &writefds))
            {
                /* If this connection has something to send */
                readynum--;
                conn->timestamp = (unsigned long)time(NULL);
                if (conn->client_context == NULL)
                    handle_http_send(conn);
                else
                    handle_https_send(conn);
                /* If close after send, remove the connection */
                if (conn->close)
                {
                    log_log(conn->conn_fd,
                        "Server closed the socket");
                    temp = conn->next;
                    connection_list_remove(connection_list,
                        conn->conn_fd);
                    conn = temp;
                    continue;
                }
            }
            if (FD_ISSET(conn->conn_fd, &readfds))
            {
                /* If this connection had something to recv */
                readynum--;
                if (conn->close)
                {
                    conn = conn->next;
                    continue;
                }
                if (conn->handshaking)
                {
                    testi = SSL_accept(conn->client_context);
                    if (testi <= 0)
                    {
                        error = SSL_get_error(
                                conn->client_context, testi);
                        if (error != SSL_ERROR_WANT_READ &&
                            error != SSL_ERROR_WANT_WRITE)
                        {
                            log_log(conn->conn_fd,
                                "Error on accept_ssl_connection");
                            temp = conn->next;
                            connection_list_remove(connection_list,
                                    conn->conn_fd);
                            conn = temp;
                            continue;
                        }
                        conn = conn->next;
                        continue;
                    }
                    else
                    {
                        conn->handshaking = 0;
                        conn = conn->next;
                        continue;
                    }
                }
                conn->timestamp = (unsigned long)time(NULL);
                if (conn->client_context == NULL)
                    n = http_recv(conn);
                else
                    n = https_recv(conn);
                if (n == -1)
                {
                    temp = conn->next;
                    connection_list_remove(connection_list,
                            conn->conn_fd);
                    conn = temp;
                    continue;
                }
                else if (n == 0)
                {
                    conn = conn->next;
                    continue;
                }
                else
                {
                    conn->buf_size += n;
                    handle_connection_recv(conn);
                    generate_response(conn);
                }
            }
            conn = conn->next;
        }
    }
    close(sock);
    close(s_sock);
    connection_list_free(connection_list);
    SSL_CTX_free(ssl_context);
    return state;
}

void set_running_state(running_state new_state)
{
    state = new_state;
}
