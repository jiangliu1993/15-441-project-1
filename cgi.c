#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <dirent.h>
#include "cgi.h"
#include "file_io.h"
#include "log.h"

char* cgi_folder = NULL;
extern int http_port;
extern int https_port;

void cgi_initialize(char* folder)
{
    DIR* test_dir;

    if (cgi_folder == NULL)
        cgi_folder = (char *)malloc(strlen(folder)+1);
    if (cgi_folder == NULL)
    {
        fprintf(stderr, "No memory to record cgi folder path");
        exit(EXIT_FAILURE);
    }
    strcpy(cgi_folder, folder);
    test_dir = opendir(cgi_folder);
    if (test_dir)
    {
        closedir(test_dir);
    }
    else
    {
        fprintf(stdout, "Can't open cgi folder: %s\n", cgi_folder);
        exit(EXIT_FAILURE);
    }
}

char** create_envp(request_node* node, char is_https, int conn_fd)
{
    char** envp;
    int i, j;
    struct sockaddr_in addr;
    socklen_t addr_size;

    envp = (char **)malloc(ENVP_NUM*sizeof(char *));
    if (envp == NULL)
        return NULL;
    for (i = 0; i < ENVP_NUM - 1; i++)
    {
        envp[i] = (char *)malloc(ENVP_LEN*sizeof(char));
        if (envp[i] == NULL)
        {
            for (j = 0; j < i; j++)
                free(envp[j]);
            free(envp);
            return NULL;
        }
    }
    envp[NULL_TERMINATE] = NULL;

    strcpy(envp[GATEWAY_INTERFACE], "GATEWAY_INTERFACE=CGI/1.1");
    strcpy(envp[SERVER_PROTOCOL], "SERVER_PROTOCOL=HTTP/1.1");
    strcpy(envp[SERVER_SOFTWARE], "SERVER_SOFTWARE=Liso/1.0");
    strcpy(envp[SCRIPT_NAME], "SCRIPT_NAME=/cgi");
    if (node->body_size > 0)
    {
        sprintf(envp[CONTENT_LENGTH], "CONTENT_LENGTH=%d",
                node->body_size);
    }
    else
    {
        sprintf(envp[CONTENT_LENGTH], "CONTENT_LENGTH=%d", 0);
    }
    sprintf(envp[PATH_INFO], "PATH_INFO=%s", node->uri+4);
    sprintf(envp[QUERY_STRING], "QUERY_STRING=%s", node->query);
    if (node->method == GET)
        sprintf(envp[REQUEST_METHOD], "REQUEST_METHOD=GET");
    else if (node->method == POST)
        sprintf(envp[REQUEST_METHOD], "REQUEST_METHOD=POST");
    else if (node->method == HEAD)
        sprintf(envp[REQUEST_METHOD], "REQUEST_METHOD=HEAD");
    sprintf(envp[REQUEST_URI], "REQUEST_URI=%s", node->uri);
    if (node->close)
        sprintf(envp[HTTP_CONNECTION],"HTTP_CONNECTION=close");
    else
        sprintf(envp[HTTP_CONNECTION],"HTTP_CONNECTION=keep-alive");
    sprintf(envp[HTTP_ACCEPT], "HTTP_ACCEPT=%s",
            node->headers.accept);
    sprintf(envp[HTTP_ACCEPT_CHARSET], "HTTP_ACCEPT_CHARSET=%s",
            node->headers.accept_charset);
    sprintf(envp[HTTP_ACCEPT_ENCODING], "HTTP_ACCEPT_ENCODING=%s",
            node->headers.accept_encoding);
    sprintf(envp[HTTP_ACCEPT_LANGUAGE], "HTTP_ACCEPT_LANGUAGE=%s",
            node->headers.accept_language);
    sprintf(envp[CONTENT_TYPE], "CONTENT_TYPE=%s",
            node->headers.content_type);
    sprintf(envp[HTTP_COOKIE], "HTTP_COOKIE=%s",
            node->headers.cookie);
    sprintf(envp[HTTP_HOST], "HTTP_HOST=%s",
            node->headers.host);
    sprintf(envp[HTTP_REFERER], "HTTP_REFERER=%s",
            node->headers.referer);
    sprintf(envp[HTTP_USER_AGENT], "HTTP_USER_AGENT=%s",
            node->headers.user_agent);
    if (is_https)
    {
        sprintf(envp[SERVER_PORT], "SERVER_PORT=%d", https_port);
        sprintf(envp[HTTPS], "HTTPS=on");
    }
    else
    {
        sprintf(envp[SERVER_PORT], "SERVER_PORT=%d", http_port);
        sprintf(envp[HTTPS], "HTTPS=off");
    }
    addr_size = sizeof(struct sockaddr_in);
    if (getpeername(conn_fd, (struct sockaddr *)&addr,
        &addr_size) == 0)
    {
        sprintf(envp[REMOTE_ADDR], "REMOTE_ADDR=%s",
                inet_ntoa(addr.sin_addr));
    }
    else
    {
        strcpy(envp[REMOTE_ADDR], "REMOTE_ADDR=");
    }
    return envp;
}

int run_cgi_request(request_node* node, char is_https, int conn_fd)
{
    pid_t pid;
    char* argv[2];
    char** envp;
    int stdin_pipe[2];
    int stdout_pipe[2];

    argv[0] = (char *)malloc(MAXLINE);
    strcpy(argv[0], cgi_folder);
    strcat(argv[0], node->uri+4);
    if (!file_exist(argv[0]))
    {
        free(argv[0]);
        return 0;
    }
    argv[1] = NULL;
    envp = create_envp(node, is_https, conn_fd);
    if (envp == NULL)
    {
        /* No memory for envp, really bad */
        log_log(conn_fd, "No memory for envp");
        return 0;
    }
    if (pipe2(stdin_pipe, O_NONBLOCK) < 0)
    {
        log_log(conn_fd, "Fail to open stdin pipe for cgi");
        free_envp(envp);
        return 0;
    }
    if (pipe2(stdout_pipe, O_NONBLOCK) < 0)
    {
        log_log(conn_fd, "Fail to open stdout pipe for cgi");
        free_envp(envp);
        return 0;
    }

    pid = fork();
    if (pid < 0)
    {
        log_log(conn_fd, "Fail to fork process for cgi");
        free_envp(envp);
        return 0;
    }
    if (pid == 0)
    {
        /* Child process */
        close(stdout_pipe[0]);
        close(stdin_pipe[1]);
        dup2(stdout_pipe[1], fileno(stdout));
        dup2(stdin_pipe[0], fileno(stdin));
        if (execve(argv[0], argv, envp))
            exit(EXIT_FAILURE);
    }
    else
    {
        /* Parent process */
        log_log(conn_fd, "CGI process started");
        close(stdout_pipe[1]);
        close(stdin_pipe[0]);
        /* We can write request body to cgi in select loop.
         * It's more fair for each connection */
        node->cgi_pipe_in = stdin_pipe[1];
        node->cgi_pipe_out = stdout_pipe[0];
    }
    free_envp(envp);
    free(argv[0]);
    return 1;
}

void free_envp(char** envp)
{
    int i;

    for (i = 0; i < ENVP_NUM; i++)
        free(envp[i]);
    free(envp);
}

void handle_cgi_in(connection* conn)
{
    int n, bytes_write;
    request_node* node;

    node = conn->queue->head;
    bytes_write = node->body_size - node->body_write;
    if (DEFAULT_BUF_SIZE < bytes_write)
        bytes_write = DEFAULT_BUF_SIZE;
    n = write(node->cgi_pipe_in, node->body_buf+node->body_write,
            bytes_write);
    if (n == -1)
    {
        if (errno != EWOULDBLOCK && errno != EAGAIN)
        {
            log_log(conn->conn_fd,
                    "Error when passing request body to CGI");
            conn->close = 1;
            return;
        }
        log_log(conn->conn_fd, "CGI pipe buffer full");
        return;
    }
    node->body_write += n;
    if (node->body_write == node->body_size)
        close(node->cgi_pipe_in);
    return;
}

void handle_cgi_out(connection* conn)
{
    int n, bytes_read;
    request_node* node;

    node = conn->queue->head;
    bytes_read = DEFAULT_BUF_SIZE - node->cgi_buf_size;
    if (bytes_read == 0)
        return;
    n = read(node->cgi_pipe_out, node->cgi_buf+node->cgi_buf_size,
            bytes_read);
    if (n == -1)
    {
        if (errno != EWOULDBLOCK && errno != EAGAIN)
        {
            log_log(conn->conn_fd,
                    "Error when reading from CGI pipe");
            conn->close = 1;
            node->cgi_finish = 1;
            return;
        }
        log_log(conn->conn_fd, "CGI pipe read would block");
        return;
    }
    if (n == 0)
    {
        log_log(conn->conn_fd, "Finished reading from CGI pipe");
        close(node->cgi_pipe_out);
        if (node->close)
            conn->close = 1;
        node->cgi_finish = 1;
        return;
    }
    node->cgi_buf_size += n;
    return;
}

int forward_cgi_out(connection* conn)
{
    int n, i, error;
    request_node* node;

    node = conn->queue->head;
    if (node->cgi_buf_size == 0)
        return 1;
    if (conn->client_context == NULL)
    {
        /* Http send */
        n = send(conn->conn_fd, node->cgi_buf,
                node->cgi_buf_size, 0);
        if (n == -1)
        {
            if (errno != EWOULDBLOCK && errno != EAGAIN)
            {
                log_log(conn->conn_fd, "Error on send");
                conn->close = 1;
                return 0;
            }
            log_log(conn->conn_fd,
                    "Send buffer full before sending");
            return 1;
        }
        if (n < node->cgi_buf_size)
        {
            log_log(conn->conn_fd,
                    "Send buffer full while sending");
            for (i = n; i < node->cgi_buf_size; i++)
                node->cgi_buf[i-n] = node->cgi_buf[i];
        }
        node->cgi_buf_size -= n;
        return 1;
    }
    else
    {
        /* Https send */
        n = SSL_write(conn->client_context, node->cgi_buf,
                node->cgi_buf_size);
        if (n <= 0)
        {
            error = SSL_get_error(conn->client_context, n);
            if (error != SSL_ERROR_WANT_READ &&
                error != SSL_ERROR_WANT_WRITE)
            {
                log_log(conn->conn_fd, "Error on https send");
                conn->close = 1;
                return 0;
            }

            log_log(conn->conn_fd,
                    "Send buffer full before sending");
            return 1;
        }
        if (n < node->cgi_buf_size)
        {
            log_log(conn->conn_fd,
                    "Send buffer full while sending");
            for (i = n; i < node->cgi_buf_size; i++)
                node->cgi_buf[i-n] = node->cgi_buf[i];
        }
        node->cgi_buf_size -= n;
        return 1;
    }
}

void cgi_close()
{
    free(cgi_folder);
}
