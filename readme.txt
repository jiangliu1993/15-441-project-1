################## ECHO SERVER #####################
Simple echo server using select() to manage multiple sockets.
Use a dynamic buffer when receives data. Double the length of the buffer 
if there's not enough space.
Use a loop when sends data. Make sure all data received will be sent.

################## HTTP SERVER #####################

This is a http server based on select.
It is fair to every client because it recv at most 8192 bytes from a
client per select and send at most 8192 bytes to a client per select.
When it recv some data, the http parser will parse the requests in it.
If a request is not completely received, it will be parsed correctly in
the next round of select.

For each completely parsed request, a response is build.
If it request a file bigger than 8192 bytes, the file will be transfered
in 2 or more rounds of select.

If a client is idle for TIME_OUT seconds, it will be closed.
During the whole recv-parse-response process, various checks
(such as length checks) are set to prevent the malicious client.

Below is some design details:
1.If a method is not implemented, 501 will be returned and connection will
be closed because it's an unimplented method, the server don't know whether
it has a body and can't parse the next pipelined request.

2.If the server fails halfway reading the requested file, it will simply
close the connection, because 200 was already sent to the client and can't
be taken back. A way to solve this is to read the whole file into memory
first. However, the server should be fair to every connection and should
not spend a huge amount of time reading a file for only one client.
Because of that, I insist my design.

3.If there is a "/../" in the requested uri, the response will be
redirected to "/index.html".(Be polite to even bad guys)

################## SSL & CGI #####################

For SSL, openssl libs are used. Sockets are wrapped by SSL context and
added to the normal select loop. If some socket is ready to read or write,
the server will use SSL_read/SSL_write instead of recv/send if the socket
has a SSl context. All other things are the same as normal http requests and
responses.

For CGI, any request uri begin with "/cgi/" will be hand to the cgi script.
CGI environment is set according to the headers of the request. After the
subprocess of CGI script has started, its stdin/stdout pipes are added to the
select loop. The reason why its stdin pipe is added to the selected loop is
that the server should not block on a CGI request with a large message body.
For each select loop, each request can pipe at most 8192 bytes to the cgi
script and read at most 8192 bytes from the cgi script, so no single client
will be starved.

For daemonize, the example code is followed. Just add a global variable for
select loop so we can gracefully terminate the server.
