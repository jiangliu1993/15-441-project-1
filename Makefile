CC = gcc
CFLAGS = -g -Wall -Werror
LSSL = -lssl

all: hcnttpd

hcnttpd.o: hcnttpd.c ; $(CC) $(CFLAGS) -c hcnttpd.c

connection.o: connection.c ; $(CC) $(CFLAGS) -c connection.c

log.o: log.c ; $(CC) $(CFLAGS) -c log.c

request.o: request.c ; $(CC) $(CFLAGS) -c request.c

response.o: response.c ; $(CC) $(CFLAGS) -c response.c

file_io.o: file_io.c ; $(CC) $(CFLAGS) -c file_io.c

cgi.o: cgi.c ; $(CC) $(CFLAGS) -c cgi.c

hcnttpd: hcnttpd.o connection.o log.o request.o response.o file_io.o cgi.o
	gcc hcnttpd.o connection.o log.o request.o response.o file_io.o cgi.o $(LSSL) -o hcnttpd
clean: ; rm hcnttpd *.o
