#include <time.h>
#include <string.h>
#include <sys/socket.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "log.h"
#include "response.h"
#include "file_io.h"
#include "cgi.h"

void handle_http_send(connection* conn)
{
    request_node* node;
    int bytes_sent;

    bytes_sent = 0;
    while (bytes_sent < DEFAULT_BUF_SIZE && conn->close == 0)
    {
        node = conn->queue->head;
        if (!node || node->stage != READY)
            break;
        /* Send response lines */
        if (http_send_response_lines(conn, node, &bytes_sent) == 0)
            return;
        /* Send the message body of the response */
        if (http_send_response_body(conn, node, &bytes_sent) == 0)
            return;
        /* Should close conn when finish sending node with close */
        if (node->close)
        {
            conn->close = 1;
            return;
        }
        node = request_queue_pop(conn->queue);
        request_node_free(node);
    }
}

int http_send_response_lines(connection* conn,
        request_node* node, int* bytes_sent)
{
    response_line* line;
    int line_len, testi, i;
    line = node->head;
    while (line)
    {
        line_len = strlen(line->data);
        testi = send(conn->conn_fd, line->data, line_len, 0);
        if (testi == -1)
        {
            if (errno != EWOULDBLOCK && errno != EAGAIN)
            {
                log_log(conn->conn_fd, "Error on send");
                conn->close = 1;
                return 0;
            }
            log_log(conn->conn_fd,
                    "Send buffer full before sending");
            return 0;
        }
        if (testi < line_len)
        {
            log_log(conn->conn_fd,
                    "Send buffer full while sending");
            for (i = testi; i <= line_len; i++)
                line->data[i-testi] = line->data[i];
            return 0;
        }
        *bytes_sent += line_len;
        line = request_node_pop(node);
        free(line);
        line = node->head;
    }
    if (*bytes_sent >= DEFAULT_BUF_SIZE)
    {
        if (node->head == NULL && node->file_fp == NULL)
        {
            if (node->close)
                conn->close = 1;
            node = request_queue_pop(conn->queue);
            request_node_free(node);
        }
        return 0;
    }

    return 1;
}

int http_send_response_body(connection*conn,
        request_node* node, int* bytes_sent)
{
    int testi;
    char file_buf[DEFAULT_BUF_SIZE];

    if (node->file_fp)
    {
        testi = fread(file_buf, 1, 
            DEFAULT_BUF_SIZE - *bytes_sent, node->file_fp);
        if (testi < DEFAULT_BUF_SIZE - *bytes_sent)
        {
            if (feof(node->file_fp))
            {
                testi = send(conn->conn_fd, file_buf,
                    node->file_meta.st_size-node->file_read,0);
                if (testi == -1)
                {
                    if (errno != EWOULDBLOCK &&
                        errno != EAGAIN)
                    {
                        log_log(conn->conn_fd, "Error on send");
                        conn->close = 1;
                        return 0;
                    }
                    log_log(conn->conn_fd,
                            "Send buffer full before sending");
                    fseek(node->file_fp, node->file_read,
                        SEEK_SET);
                    return 0;
                }
                if (testi < node->file_meta.st_size -
                    node->file_read)
                {
                    log_log(conn->conn_fd,
                            "Send buffer full while sending");
                    node->file_read += testi;
                    fseek(node->file_fp, node->file_read,
                        SEEK_SET);
                    return 0;
                }
                *bytes_sent += testi;
                node->file_read = node->file_meta.st_size;
            }
            else
            {
                log_log(conn->conn_fd, "Read file error");
                conn->close = 1;
                return 0;
            }
        }
        else
        {
            testi = send(conn->conn_fd, file_buf,
                DEFAULT_BUF_SIZE - *bytes_sent, 0);
            if (testi == -1)
            {
                if (errno != EWOULDBLOCK &&
                    errno != EAGAIN)
                {
                    log_log(conn->conn_fd, "Error on send");
                    conn->close = 1;
                    return 0;
                }
                log_log(conn->conn_fd,
                        "Send buffer full before sending");
                fseek(node->file_fp, node->file_read,
                    SEEK_SET);
                return 0;
            }
            if (testi < DEFAULT_BUF_SIZE - *bytes_sent)
            {
                log_log(conn->conn_fd,
                        "Send buffer full while sending");
                node->file_read += testi;
                fseek(node->file_fp, node->file_read,
                    SEEK_SET);
                return 0;
            }
            node->file_read += testi;
            *bytes_sent = DEFAULT_BUF_SIZE;
        }
        if (*bytes_sent >= DEFAULT_BUF_SIZE)
        {
            if (node->file_meta.st_size == node->file_read)
            {
                if (node->close)
                    conn->close = 1;
                node = request_queue_pop(conn->queue);
                request_node_free(node);
            }
            return 0;
        }
    }
    return 1;
}

void handle_https_send(connection* conn)
{
    request_node* node;
    int bytes_sent;

    bytes_sent = 0;
    while (bytes_sent < DEFAULT_BUF_SIZE && conn->close == 0)
    {
        node = conn->queue->head;
        if (!node)
            break;
        /* Send response lines */
        if (https_send_response_lines(conn, node, &bytes_sent) == 0)
            return;
        /* Send the message body of the response */
        if (https_send_response_body(conn, node, &bytes_sent) == 0)
            return;
        /* Should close conn when finish sending node with close */
        if (node->close)
        {
            conn->close = 1;
            return;
        }
        node = request_queue_pop(conn->queue);
        request_node_free(node);
    }
}

int https_send_response_lines(connection* conn,
        request_node* node, int* bytes_sent)
{
    response_line* line;
    int line_len, testi, i, error;
    line = node->head;
    while (line)
    {
        line_len = strlen(line->data);
        if (line_len == 0)
        {
            line = request_node_pop(node);
            free(line);
            line = node->head;
            continue;
        }
        testi = SSL_write(conn->client_context,
                line->data, line_len);
        if (testi <= 0)
        {
            error = SSL_get_error(conn->client_context, testi);
            if (error != SSL_ERROR_WANT_READ &&
                error != SSL_ERROR_WANT_WRITE)
            {
                log_log(conn->conn_fd, "Error on https send");
                conn->close = 1;
                return 0;
            }

            log_log(conn->conn_fd,
                    "Send buffer full before sending");
            return 0;
        }
        if (testi < line_len)
        {
            log_log(conn->conn_fd,
                    "Send buffer full while sending");
            for (i = testi; i <= line_len; i++)
                line->data[i-testi] = line->data[i];
            return 0;
        }
        *bytes_sent += line_len;
        line = request_node_pop(node);
        free(line);
        line = node->head;
    }
    if (*bytes_sent >= DEFAULT_BUF_SIZE)
    {
        if (node->head == NULL && node->file_fp == NULL)
        {
            if (node->close)
                conn->close = 1;
            node = request_queue_pop(conn->queue);
            request_node_free(node);
        }
        return 0;
    }

    return 1;
}

int https_send_response_body(connection*conn,
        request_node* node, int* bytes_sent)
{
    int testi, error;
    char file_buf[DEFAULT_BUF_SIZE];

    if (node->file_fp)
    {
        testi = fread(file_buf, 1, 
            DEFAULT_BUF_SIZE - *bytes_sent, node->file_fp);
        if (testi < DEFAULT_BUF_SIZE - *bytes_sent)
        {
            if (feof(node->file_fp))
            {
                if (node->file_meta.st_size-node->file_read == 0)
                    return 1;
                testi = SSL_write(conn->client_context, file_buf,
                        node->file_meta.st_size-node->file_read);
                if (testi <= 0)
                {
                    error = SSL_get_error(conn->client_context,
                            testi);
                    if (error != SSL_ERROR_WANT_READ &&
                        error != SSL_ERROR_WANT_WRITE)
                    {
                        log_log(conn->conn_fd,
                                "Error on https send");
                        conn->close = 1;
                        return 0;
                    }

                    log_log(conn->conn_fd,
                            "Send buffer full before sending");
                    fseek(node->file_fp, node->file_read,
                        SEEK_SET);
                    return 0;
                }
                if (testi < node->file_meta.st_size -
                    node->file_read)
                {
                    log_log(conn->conn_fd,
                            "Send buffer full while sending");
                    node->file_read += testi;
                    fseek(node->file_fp, node->file_read,
                        SEEK_SET);
                    return 0;
                }
                *bytes_sent += testi;
                node->file_read = node->file_meta.st_size;
            }
            else
            {
                log_log(conn->conn_fd, "Read file error");
                conn->close = 1;
                return 0;
            }
        }
        else
        {
            if (DEFAULT_BUF_SIZE - *bytes_sent == 0)
            {
                if (node->file_meta.st_size == node->file_read)
                    {
                        if (node->close)
                            conn->close = 1;
                        node = request_queue_pop(conn->queue);
                        request_node_free(node);
                    }
                return 0;
            }
            testi = SSL_write(conn->client_context, file_buf,
                    DEFAULT_BUF_SIZE - *bytes_sent);
            if (testi <= 0)
            {
                error = SSL_get_error(conn->client_context,
                        testi);
                if (error != SSL_ERROR_WANT_READ &&
                    error != SSL_ERROR_WANT_WRITE)
                {
                    log_log(conn->conn_fd,
                            "Error on https send");
                    conn->close = 1;
                    return 0;
                }

                log_log(conn->conn_fd,
                        "Send buffer full before sending");
                fseek(node->file_fp, node->file_read,
                    SEEK_SET);
                return 0;
            }
            if (testi < DEFAULT_BUF_SIZE - *bytes_sent)
            {
                log_log(conn->conn_fd,
                        "Send buffer full while sending");
                node->file_read += testi;
                fseek(node->file_fp, node->file_read,
                    SEEK_SET);
                return 0;
            }
            node->file_read += testi;
            *bytes_sent = DEFAULT_BUF_SIZE;
        }
        if (*bytes_sent >= DEFAULT_BUF_SIZE)
        {
            if (node->file_meta.st_size == node->file_read)
            {
                if (node->close)
                    conn->close = 1;
                node = request_queue_pop(conn->queue);
                request_node_free(node);
            }
            return 0;
        }
    }
    return 1;
}

void generate_service_unavailable_response(connection* conn)
{
    request_node* node;

    node = create_request_node();
    if (node == NULL)
    {
        log_log(conn->conn_fd,
            "No memory to reply SERVICE_UNAVAILABLE");
        conn->close = 1;
        return;
    }
    node->stage = ABORT;
    node->status_code = SERVICE_UNAVAILABLE;
    node->close = 1;
    request_queue_add_node(conn->queue, node);

    generate_response(conn);
}

void generate_time_out_response(connection* conn)
{
    request_node* node;

    node = create_request_node();
    if (node == NULL)
    {
        log_log(conn->conn_fd,
            "No memory to build time out response");
        conn->close = 1;
        return;
    }
    node->stage = ABORT;
    node->status_code = REQUEST_TIME_OUT;
    node->close = 1;
    request_queue_add_node(conn->queue, node);

    generate_response(conn);
}

/* Generate responses for the connection's queued requests */
void generate_response(connection* conn)
{
    request_node* node;

    node = conn->queue->head;
    while (node)
    {
        if (node->stage == ABORT)
        {
            if (node->method == NONE)
            {
                log_log(conn->conn_fd,
                    "Aborted");
            }
            else if (node->method == GET)
            {
                log_log(conn->conn_fd,
                    "Malformed GET %s - %s",
                        node->uri, node->headers.user_agent);
            }
            else if (node->method == POST)
            {
                log_log(conn->conn_fd,
                    "Malformed POST %s - %s",
                        node->uri, node->headers.user_agent);
            }
            else if (node->method == HEAD)
            {
                log_log(conn->conn_fd,
                    "Malformed HEAD %s - %s",
                        node->uri, node->headers.user_agent);
            }
            generate_error_response(node);
            node->stage = READY;
        }
        else if (node->stage == DONE)
        {
            if (node->method == NONE)
                log_log(conn->conn_fd, "Should never reach here");
            else if (node->method == GET)
            {
                log_log(conn->conn_fd,
                    "GET %s - %s", node->uri,
                        node->headers.user_agent);
            }
            else if (node->method == POST)
            {
                log_log(conn->conn_fd,
                    "POST %s - %s", node->uri,
                        node->headers.user_agent);
            }
            else if (node->method == HEAD)
            {
                log_log(conn->conn_fd,
                    "HEAD %s - %s", node->uri,
                        node->headers.user_agent);
            }

            parse_node_uri(node);
            if (strstr(node->uri, "/cgi/") == node->uri)
            {
                /* Deal with cgi request */
                if (conn->client_context == NULL)
                {
                    if (run_cgi_request(node, 0, conn->conn_fd))
                    {
                        node->stage = CGI;
                        if (node->body_size <= 0)
                            close(node->cgi_pipe_in);
                    }
                    else
                    {
                        node->status_code = INTERNAL_SERVER_ERROR;
                        node->stage = ABORT;
                        generate_error_response(node);
                        node->stage = READY;
                    }
                }
                else
                {
                    if (run_cgi_request(node, 1, conn->conn_fd))
                    {
                        node->stage = CGI;
                        if (node->body_size <= 0)
                            close(node->cgi_pipe_in);
                    }
                    else
                    {
                        node->status_code = INTERNAL_SERVER_ERROR;
                        node->stage = ABORT;
                        generate_error_response(node);
                        node->stage = READY;
                    }
                }
            }
            else
            {
                generate_normal_response(node);
                node->stage = READY;
            }
        }
        if (node->close)
            break;
        node = node->next;
    }
}

void generate_normal_response(request_node* node)
{
    if (get_file(node) == 0)
    {
        node->status_code = NOT_FOUND;
        generate_error_response(node);
        return;
    }
    if(node->method == GET)
    {
        node->status_code = OK;
        generate_get_response_headers(node);
    }
    else if (node->method == HEAD)
    {
        node->status_code = OK;
        generate_get_response_headers(node);
    }
    else if (node->method == POST)
    {
        node->status_code = OK;
        generate_get_response_headers(node);
    }
}

void generate_get_response_headers(request_node* node)
{
    char status_line[MAXLINE], content_len[MAXLINE];
    char content_type[MAXLINE];
    char date_buf[MAXLINE], last_modified_buf[MAXLINE];
    time_t current_time;
    struct tm *gmt;
    response_line* temp_line;
        
    sprintf(status_line, "HTTP/1.1 %d %s\r\n",
       node->status_code, "OK");
    sprintf(content_len, "Content-Length: %d\r\n",
        (int)node->file_meta.st_size);
    temp_line = create_response_line(status_line);
    if (temp_line == NULL)
    {
        node->close = 1;
        return;
    }
    request_node_add_line(node, temp_line);
    temp_line = create_response_line(content_len);
    if (temp_line == NULL)
    {
        node->close = 1;
        return;
    }
    request_node_add_line(node, temp_line);
    if (node->close)
    {
        temp_line = create_response_line("Connection: close\r\n");
        if (temp_line == NULL)
            return;
        request_node_add_line(node, temp_line);
    }
    else
    {
        temp_line = create_response_line("Connection: keep-alive\r\n");
        if (temp_line == NULL)
            return;
        request_node_add_line(node, temp_line);
    }
        
    temp_line = create_response_line("Server: Liso/1.0\r\n");
    if (temp_line == NULL)
    {
        node->close = 1;
        return;
    }
    request_node_add_line(node, temp_line);
    /* Generate Content-Type corresponding to the extension */
    generate_content_type(node->uri, content_type);
    temp_line = create_response_line(content_type);
    if (temp_line == NULL)
    {
        node->close = 1;
        return;
    }
    request_node_add_line(node, temp_line);
    current_time = time(NULL);
    gmt = gmtime(&current_time);
    if (gmt != NULL)
    {
        if (strftime(date_buf, sizeof(date_buf),
            "Date: %a, %d %b %Y %H:%M:%S %Z\r\n", gmt) != 0)
        {
            temp_line = create_response_line(date_buf);
            if (temp_line == NULL)
            {
                node->close = 1;
                return;
            }
            request_node_add_line(node, temp_line);
        }
    }
    /* Last modified time */
    gmt = gmtime(&(node->file_meta.st_mtime));
    if (gmt != NULL)
    {
        if (strftime(last_modified_buf, sizeof(last_modified_buf),
            "Last-Modified: %a, %d %b %Y %H:%M:%S %Z\r\n", gmt)!=0)
        {
            temp_line = create_response_line(last_modified_buf);
            if (temp_line == NULL)
            {
                node->close = 1;
                return;
            }
            request_node_add_line(node, temp_line);
        }
    }
    temp_line = create_response_line("\r\n");
    if (temp_line == NULL)
    {
        node->close = 1;
        return;
    }
    request_node_add_line(node, temp_line);
}

void generate_content_type(char* uri, char* buf)
{
    char* extension;

    extension = strrchr(uri, '.');
    if (extension == NULL)
        strcpy(buf, "Content-Type: application/octet-stream\r\n");
    else if (strcmp(extension, ".html") == 0)
        strcpy(buf, "Content-Type: text/html\r\n");
    else if (strcmp(extension, ".css") == 0)
        strcpy(buf, "Content-Type: text/css\r\n");
    else if (strcmp(extension, ".png") == 0)
        strcpy(buf, "Content-Type: image/png\r\n");
    else if (strcmp(extension, ".jpg") == 0)
        strcpy(buf, "Content-Type: image/jpeg\r\n");
    else if (strcmp(extension, ".gif") == 0)
        strcpy(buf, "Content-Type: image/gif\r\n");
    else
        strcpy(buf, "Content-Type: application/octet-stream\r\n");
}

void generate_error_response(request_node* node)
{
    char body[MAXLINE], status_line[MAXLINE], content_len[MAXLINE];
    char date_buf[MAXLINE];
    time_t current_time;
    struct tm *gmt;
    response_line* temp_line;

    sprintf(body, "<html><title>Server Error</title>");    
    if (node->status_code == BAD_REQUEST)
    {
        sprintf(status_line, "HTTP/1.1 %d %s\r\n",
            node->status_code, "Bad Request");
        sprintf(body, "%s%d: %s\r\n", body, node->status_code,
            "Bad request, can't parse method or uri or version");
    }
    else if (node->status_code == NOT_FOUND)
    {
        sprintf(status_line, "HTTP/1.1 %d %s\r\n",
            node->status_code, "Not Found");
        sprintf(body, "%s%d: %s\r\n", body, node->status_code,
            "Request content not found");
    }
    else if (node->status_code == REQUEST_TIME_OUT)
    {
        sprintf(status_line, "HTTP/1.1 %d %s\r\n",
            node->status_code, "Request Time-out");
        sprintf(body, "%s%d: %s\r\n", body, node->status_code,
            "Request time out");
    }
    else if (node->status_code == LENGTH_REQUIRED)
    {
        sprintf(status_line, "HTTP/1.1 %d %s\r\n",
            node->status_code, "Length Required");
        sprintf(body, "%s%d: %s\r\n", body, node->status_code,
            "Content-Length required for POST");
    }
    else if (node->status_code == REQUEST_TOO_LARGE)
    {
        sprintf(status_line, "HTTP/1.1 %d %s\r\n",
            node->status_code, "Request Entity Too Large");
        sprintf(body, "%s%d: %s\r\n", body, node->status_code,
            "Request too large or request line too long");
    }
    else if (node->status_code == URI_TOO_LARGE)
    {
        sprintf(status_line, "HTTP/1.1 %d %s\r\n",
            node->status_code, "Request-URI too large");
        sprintf(body, "%s%d: %s\r\n", body, node->status_code,
            "Request uri larger than server can handle");
    }
    else if (node->status_code == INTERNAL_SERVER_ERROR)
    {
        sprintf(status_line, "HTTP/1.1 %d %s\r\n",
            node->status_code, "Internal Server Error");
        sprintf(body, "%s%d: %s\r\n", body, node->status_code,
            "Some bugs have crawled into the server");
    }
    else if (node->status_code == NOT_IMPLEMENTED)
    {
        sprintf(status_line, "HTTP/1.1 %d %s\r\n",
            node->status_code, "Not Implemented");
        sprintf(body, "%s%d: %s\r\n", body, node->status_code,
            "Method not implemented");
    }
    else if (node->status_code == SERVICE_UNAVAILABLE)
    {
        sprintf(status_line, "HTTP/1.1 %d %s\r\n",
            node->status_code, "Service unavailable");
        sprintf(body, "%s%d: %s\r\n", body, node->status_code,
            "Server under maintenance or too many connections");
    }
    else if (node->status_code == VERSION_NOT_SUPPORT)
    {
        sprintf(status_line, "HTTP/1.1 %d %s\r\n",
            node->status_code, "HTTP Version not supported");
        sprintf(body, "%s%d: %s\r\n", body, node->status_code,
            "HTTP version not support by server");
    }
    else
    {
        sprintf(status_line, "HTTP/1.1 %d %s\r\n",
            node->status_code, "Unknown error");
        sprintf(body, "%s%d: %s\r\n", body, node->status_code,
            "Unknown error");
    }
    sprintf(content_len, "Content-Length: %d\r\n",
        (int)strlen(body));

    temp_line = create_response_line(status_line);
    if (temp_line == NULL)
    {
        node->close = 1;
        return;
    }
    request_node_add_line(node, temp_line);
    temp_line = create_response_line(content_len);
    if (temp_line == NULL)
    {
        node->close = 1;
        return;
    }
    request_node_add_line(node, temp_line);
    if (node->close)
    {
        temp_line = create_response_line("Connection: close\r\n");
        if (temp_line == NULL)
            return;
        request_node_add_line(node, temp_line);
    }
    else
    {
        temp_line = create_response_line("Connection: keep-alive\r\n");
        if (temp_line == NULL)
            return;
        request_node_add_line(node, temp_line);
    }
    temp_line = create_response_line("Server: Liso/1.0\r\n");
    if (temp_line == NULL)
    {
        node->close = 1;
        return;
    }
    request_node_add_line(node, temp_line);
    temp_line = create_response_line("Content-Type: text/html\r\n");
    if (temp_line == NULL)
    {
        node->close = 1;
        return;
    }
    request_node_add_line(node, temp_line);

    current_time = time(NULL);
    gmt = gmtime(&current_time);
    if (gmt != NULL)
    {
        if (strftime(date_buf, sizeof(date_buf),
            "Date: %a, %d %b %Y %H:%M:%S %Z\r\n", gmt) != 0)
        {
            temp_line = create_response_line(date_buf);
            if (temp_line == NULL)
            {
                node->close = 1;
                return;
            }
            request_node_add_line(node, temp_line);
        }
    }
    temp_line = create_response_line("\r\n");
    if (temp_line == NULL)
    {
        node->close = 1;
        return;
    }
    request_node_add_line(node, temp_line);
    /* View error body as a header to simplify design */
    temp_line = create_response_line(body);
    if (temp_line == NULL)
    {
        node->close = 1;
        return;
    }
    request_node_add_line(node, temp_line);
}

response_line* create_response_line(char* buf)
{
    response_line* line;

    line = (response_line *)malloc(sizeof(response_line));
    if (line == NULL)
    {
        log_log(-1, "No memory to create response line");
        return NULL;
    }
    strcpy(line->data, buf);
    line->next = NULL;

    return line;
}

response_line* request_node_pop(request_node* node)
{
    response_line* line;

    if (node->head == NULL)
        return NULL;
    line = node->head;
    if (line->next == NULL)
    {
        node->head = NULL;
        node->tail = NULL;
    }
    else
    {
        node->head = line->next;
    }
    return line;
}

void request_node_add_line(request_node* node, response_line* line)
{
    if (node->head == NULL)
    {
        node->head = line;
        node->tail = line;
    }
    else
    {
        node->tail->next = line;
        node->tail = line;
    }
}
