#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "request.h"
#include "response.h"
#include "log.h"
#include "file_io.h"


request_node* create_request_node()
{
    request_node* node;

    node = (request_node *)malloc(sizeof(request_node));
    if (node == NULL)
        return NULL;
    node->method = NONE;
    node->stage = START;
    node->uri[0] = '\0';
    node->query[0] = '\0';
    node->headers.accept[0] = '\0';
    node->headers.accept_charset[0] = '\0';
    node->headers.accept_encoding[0] = '\0';
    node->headers.accept_language[0] = '\0';
    node->headers.content_type[0] = '\0';
    node->headers.cookie[0] = '\0';
    node->headers.host[0] = '\0';
    node->headers.referer[0] = '\0';
    node->headers.user_agent[0] = '\0';
    node->close = 0;
    node->header_num = 0;
    node->body_size = -1;
    node->body_read = 0;
    node->body_write = 0;
    node->body_buf = NULL;
    node->status_code = 0;
    node->head = NULL;
    node->tail = NULL;
    node->cgi_pipe_in = -1;
    node->cgi_pipe_out = -1;
    node->cgi_buf_size = 0;
    node->cgi_finish = 0;
    node->file_fp = NULL;
    node->file_read = 0;
    node->next = NULL;

    return node;
}

void request_node_free(request_node* node)
{
    response_line* line;
    response_line* temp;

    if (node->body_buf != NULL)
        free(node->body_buf);
    if (node->file_fp != NULL)
        fclose(node->file_fp);
    line = node->head;
    while (line)
    {
        temp = line->next;
        free(line);
        line = temp;
    }
    free(node);
}

request_queue* create_request_queue()
{
    request_queue* queue;

    queue = (request_queue *)malloc(sizeof(request_queue));
    if (queue == NULL)
        return NULL;
    queue->head = NULL;
    queue->tail = NULL;

    return queue;
}

void request_queue_add_node(request_queue* queue, request_node* node)
{
    if (queue->head == NULL)
    {
        queue->head = node;
        queue->tail = node;
    }
    else
    {
        queue->tail->next = node;
        queue->tail = node;
    }
}

request_node* request_queue_pop(request_queue* queue)
{
    request_node* node;

    if (queue->head == NULL)
    {
        return NULL;
    }
    node = queue->head;
    if (node->next == NULL)
    {
        queue->head = NULL;
        queue->tail = NULL;
    }
    else
    {
        queue->head = node->next;
    }
    return node;
}

void request_queue_free(request_queue* queue)
{
    request_node* node;
    request_node* temp;

    node = queue->head;
    while (node)
    {
        temp = node->next;
        request_node_free(node);
        node = temp;
    }
    free(queue);
}

/* Handle the current received data of this connection */
void handle_connection_recv(connection* conn)
{
    char line_buf[MAXLINE];

    while (1)
    {
        if (conn->current_request == NULL)
        {
            /* Start building a new request */
            conn->current_request = create_request_node();
            if (conn->current_request == NULL)
            {
                log_log(conn->conn_fd, "No memory for new request");
                conn->close = 1;
                return;
            }
        }
        if (conn->current_request->stage == ABORT)
        {
            /* Abort all the left message in the request */
            request_queue_add_node(conn->queue,
                    conn->current_request);
            conn->current_request = NULL;
            return;
        }
        if (conn->current_request->stage == BODY)
        {
            /* Receive message body of request */
            read_message_body(conn);
            if (conn->current_request->stage == DONE)
            {
                /* Add request to queue */
                request_queue_add_node(conn->queue,
                        conn->current_request);
                conn->current_request = NULL;
            }
            if(conn->buf_size == 0)
                break;
            continue;
        }
        if (connection_readline(conn, line_buf) == 0)
        {
            /* Can't read more lines from current buffer */
            if (conn->buf_size == DEFAULT_BUF_SIZE)
            {
                /* request head length > 8192 */
                conn->current_request->status_code =
                    REQUEST_TOO_LARGE;
                conn->current_request->stage = ABORT;
                conn->current_request->close = 1;
                continue;
            }
            if (conn->buf_size > 0)
                log_log(conn->conn_fd, "Separated request");
            break;
        }
        if (conn->current_request->stage == START)
        {
            /* Parse the start line of the request */
            parse_request_line(conn->current_request, line_buf);
        }
        else if (conn->current_request->stage == HEADER)
        {
            /* Parse the headers of the request */
            parse_request_header(conn->current_request, line_buf);
            if (conn->current_request->stage == DONE)
            {
                /* Add request to queue */
                request_queue_add_node(conn->queue,
                        conn->current_request);
                conn->current_request = NULL;
            }
        }
    }
}

/* Read message body of a request */
void read_message_body(connection* conn)
{
    request_node* node;
    int bytes_need, i;

    node = conn->current_request;
    if (node->body_buf == NULL)
        node->body_buf = (char *)malloc(node->body_size);
    if (node->body_buf == NULL)
    {
        log_log(conn->conn_fd, "No memory to store request body");
        node->status_code = INTERNAL_SERVER_ERROR;
        node->stage = ABORT;
        node->close = 1;
        return;
    }
    bytes_need = node->body_size - node->body_read;
    if (bytes_need <= conn->buf_size)
    {
        memcpy(node->body_buf + node->body_read, conn->buf, bytes_need);
        node->body_read += bytes_need;
        for (i = bytes_need; i < conn->buf_size; i++)
            conn->buf[i-bytes_need] = conn->buf[i];
        conn->buf_size = conn->buf_size - bytes_need;
        node->stage = DONE;
    }
    else
    {
        memcpy(node->body_buf + node->body_read, conn->buf, conn->buf_size);
        node->body_read += conn->buf_size;
        conn->buf_size = 0;
    }
}

/* Read a line end with \r\n from the connection buffer */
int connection_readline(connection* conn, char* line_buf)
{
    int i, j;

    for (i = 1; i < conn->buf_size; i++)
    {
        if (conn->buf[i-1] == '\r' && conn->buf[i] == '\n')
        {
            /* Read a line */
            memcpy(line_buf, conn->buf, i-1);
            line_buf[i-1] = '\0';
            /* Shift buffer to new data */
            for (j = i+1; j < conn->buf_size; j++)
                conn->buf[j-i-1] = conn->buf[j];
            conn->buf_size = conn->buf_size - i - 1;
            return 1;
        }
    }

    return 0;
}

/* Parse the request line of the request */
void parse_request_line(request_node* node, char* line_buf)
{
    int n;
    char method[MAXLINE], uri[MAXLINE], version[MAXLINE];

    n = sscanf(line_buf, "%s %s %s", method, uri, version);
    if (n != 3)
    {
        /* Bad request line */
        node->method = NONE;
        node->status_code = BAD_REQUEST;
        node->stage = ABORT;
        node->close = 1;
    }
    else
    {
        /* Parse request method */
        if (strstr(method, "GET") == method)
            node->method = GET;
        else if (strstr(method, "POST") == method)
            node->method = POST;
        else if (strstr(method, "HEAD") == method)
            node->method = HEAD;
        else
        {
            node->method = NONE;
            node->status_code = NOT_IMPLEMENTED;
            node->stage = ABORT;
            node->close = 1;
            return;
        }
        /* Parse request version */
        if (strstr(version, "HTTP/1.1") != version)
        {
            node->status_code = VERSION_NOT_SUPPORT;
            node->stage = ABORT;
            node->close = 1;
            return;
        }
        if (strlen(uri) > MAX_PATH_LENGTH - 100)
        {
            node->status_code = URI_TOO_LARGE;
            node->stage = ABORT;
            node->close = 1;
            return;
        }
        strcpy(node->uri, uri);
        node->stage = HEADER;
    }
}

/* Parse the headers of the request */
void parse_request_header(request_node* node, char* line_buf)
{
    char token[MAXLINE];
    if (line_buf[0] == '\0')
    {
        /* \r\n read, current request ends */
        if (node->body_size > 0)
        {
            /* More message body data need to be received */
            node->stage = BODY;
            return;
        }
        else
        {
            /* Finish current request, put into queue */
            if (node->method == POST && node->body_size == -1)
            {
                node->status_code = LENGTH_REQUIRED;
                node->stage = ABORT;
                node->close = 1;
                return;
            }
            /* Request parsing finished */
            node->stage = DONE;
            return;
        }
    }
    node->header_num++;
    if (node->header_num > MAX_HEADER_NUM)
    {
        node->status_code = REQUEST_TOO_LARGE;
        node->stage = ABORT;
        node->close = 1;
        return;
    }
    if (strstr(line_buf, "Connection:") == line_buf)
    {
        sscanf(line_buf, "Connection: %s", token);
        if (strcmp(token, "close") == 0 ||
            strcmp(token, "Close") == 0)
            node->close = 1;
    }
    else if (strstr(line_buf, "Content-Length:") == line_buf)
    {
        sscanf(line_buf, "Content-Length: %d",&(node->body_size));
        if (node->body_size > MAX_BODY_SIZE)
        {
            node->status_code = REQUEST_TOO_LARGE;
            node->stage = ABORT;
            node->close = 1;
            return;
        }
        if (node->body_size < 0)
        {
            node->status_code = BAD_REQUEST;
            node->stage = ABORT;
            node->close = 1;
            return;
        }
    }
    else if (strstr(line_buf, "Accept:") == line_buf)
    {
        sscanf(line_buf, "Accept: %[^\r^\n]",
                node->headers.accept);
    }
    else if (strstr(line_buf, "Accept-Charset:") == line_buf)
    {
        sscanf(line_buf, "Accept-Charset: %[^\r^\n]",
                node->headers.accept_charset);
    }
    else if (strstr(line_buf, "Accept-Encoding:") == line_buf)
    {
        sscanf(line_buf, "Accept-Encoding: %[^\r^\n]",
                node->headers.accept_encoding);
    }
    else if (strstr(line_buf, "Accept-Language:") == line_buf)
    {
        sscanf(line_buf, "Accept-Language: %[^\r^\n]",
                node->headers.accept_language);
    }
    else if (strstr(line_buf, "Content-Type:") == line_buf)
    {
        sscanf(line_buf, "Content-Type: %[^\r^\n]",
                node->headers.content_type);
    }
    else if (strstr(line_buf, "Cookie:") == line_buf)
    {
        sscanf(line_buf, "Cookie: %[^\r^\n]",
                node->headers.cookie);
    }
    else if (strstr(line_buf, "Host:") == line_buf)
    {
        sscanf(line_buf, "Host: %[^\r^\n]",
                node->headers.host);
    }
    else if (strstr(line_buf, "Referer:") == line_buf)
    {
        sscanf(line_buf, "Referer: %[^\r^\n]",
                node->headers.referer);
    }
    else if (strstr(line_buf, "User-Agent:") == line_buf)
    {
        sscanf(line_buf, "User-Agent: %[^\r^\n]",
                node->headers.user_agent);
    }
    else if (!strchr(line_buf, ':'))
    {
        node->status_code = BAD_REQUEST;
        node->stage = ABORT;
        node->close = 1;
        return;
    }
}
