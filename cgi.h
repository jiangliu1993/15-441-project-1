#ifndef CGI_H
#define CGI_H

#include "request.h"
#include "connection.h"

#define CONTENT_LENGTH 0
#define CONTENT_TYPE 1
#define GATEWAY_INTERFACE 2
#define PATH_INFO 3
#define QUERY_STRING 4
#define REMOTE_ADDR 5
#define REQUEST_METHOD 6
#define REQUEST_URI 7
#define SCRIPT_NAME 8
#define SERVER_PORT 9
#define SERVER_PROTOCOL 10
#define SERVER_SOFTWARE 11
#define HTTP_ACCEPT 12
#define HTTP_REFERER 13
#define HTTP_ACCEPT_ENCODING 14
#define HTTP_ACCEPT_LANGUAGE 15
#define HTTP_ACCEPT_CHARSET 16
#define HTTP_COOKIE 17
#define HTTP_USER_AGENT 18
#define HTTP_CONNECTION 19
#define HTTP_HOST 20
#define HTTPS 21
#define NULL_TERMINATE 22

#define ENVP_NUM 23
#define ENVP_LEN 8220

void cgi_initialize(char* folder);
char** create_envp(request_node* node, char is_https, int conn_fd);
int run_cgi_request(request_node* node, char is_https, int conn_fd);
void free_envp(char** envp);
void handle_cgi_in(connection* conn);
void handle_cgi_out(connection* conn);
int forward_cgi_out(connection* conn);
void cgi_close();

#endif
