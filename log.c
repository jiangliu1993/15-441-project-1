#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include "log.h"

FILE* log_fp = NULL;

void log_initialize(char* filename)
{
    log_fp = fopen(filename, "w");
    if (log_fp == NULL)
    {
        fprintf(stdout, "Invalid log file: %s\n", filename); 
        exit(EXIT_FAILURE);
    }
    log_log(-1, "Log start......");
}

void log_log(int conn_fd, char* str, ...)
{
    time_t log_time;
    char* log_time_str;
    va_list a_list;
    struct sockaddr_in addr;
    socklen_t addr_size;
    char conn_ip[20]; // Enough for ip xxx.xxx.xxx.xxx
    
    if (log_fp == NULL)
    {
        fprintf(stderr, "Log fail: Null pointer to log file\n");
        return;
    }

    addr_size = sizeof(struct sockaddr_in);
    if (conn_fd != -1)
    {
        if (getpeername(conn_fd, (struct sockaddr *)&addr,
            &addr_size) == 0)
        {
            strcpy(conn_ip, inet_ntoa(addr.sin_addr));
        }
        else
        {
            strcpy(conn_ip, "Unknown IP");
        }
    }
    else
    {
        strcpy(conn_ip, "General");
    }

    log_time = time(NULL);
    if (log_time != ((time_t)-1))
    {
        log_time_str = ctime(&log_time);
        if (log_time_str != NULL)
        {
            fprintf(log_fp, "%s----%s: ", log_time_str, conn_ip);
            va_start(a_list, str);
            vfprintf(log_fp, str, a_list);
            va_end(a_list);
            fprintf(log_fp, "\n");
            fflush(log_fp);
            return;
        }
    }
    fprintf(log_fp, "Unknown Time----%s: ", conn_ip);
    va_start(a_list, str);
    vfprintf(log_fp, str, a_list);
    va_end(a_list);
    fprintf(log_fp, "\n");
    fflush(log_fp);
    return;
}

void log_close()
{
    log_log(-1, "Log stop......");
    fclose(log_fp);
}
