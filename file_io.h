#ifndef FILE_IO_H
#define FILE_IO_H

#include "request.h"

#define MAX_PATH_LENGTH 4096

void file_io_initialize(char* folder);
void parse_node_uri(request_node* node);
void file_io_close();
int get_file(request_node* node);
int file_exist(char* path);

#endif
