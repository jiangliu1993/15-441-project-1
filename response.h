#ifndef RESPONSE_H
#define RESPONSE_H

#include "request.h"
#include "connection.h"

#define OK 200
#define BAD_REQUEST 400
#define NOT_FOUND 404
#define REQUEST_TIME_OUT 408
#define LENGTH_REQUIRED 411
#define REQUEST_TOO_LARGE 413
#define URI_TOO_LARGE 414
#define INTERNAL_SERVER_ERROR 500
#define NOT_IMPLEMENTED 501
#define SERVICE_UNAVAILABLE 503
#define VERSION_NOT_SUPPORT 505

struct response_line {
    char data[MAXLINE];
    struct response_line* next;
};

void handle_http_send(connection* conn);
void handle_https_send(connection* conn);
int http_send_response_lines(connection* conn,
        request_node* node, int* sent);
int http_send_response_body(connection*conn,
        request_node* node, int* bytes_sent);
int https_send_response_lines(connection* conn,
        request_node* node, int* sent);
int https_send_response_body(connection*conn,
        request_node* node, int* bytes_sent);

void generate_response(connection* conn);
void generate_service_unavailable_response(connection* conn);
void generate_time_out_response(connection* conn);
void generate_error_response(request_node* node);
void generate_normal_response(request_node* node);
void generate_content_type(char* uri, char* buf);
void generate_get_response_headers(request_node* node);

response_line* create_response_line(char* buf);
response_line* request_node_pop(request_node* node);
void request_node_add_line(request_node* node, response_line* line);

#endif
