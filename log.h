#ifndef LOG_H
#define LOG_H

void log_initialize(char* filename);
void log_log(int conn_fd, char* str, ...);
void log_close();

#endif
