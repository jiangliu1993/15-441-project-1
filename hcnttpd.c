#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "connection.h"
#include "log.h"
#include "file_io.h"
#include "cgi.h"

void signal_handler(int sig)
{
    switch(sig)
    {
        case SIGHUP:
            /* rehash the server */
            set_running_state(RESTART);
            break;
        case SIGTERM:
            /* finalize and shutdown the server */
            set_running_state(STOP);
            break;
        default:
            break;
    }
}

void daemonize(char* lock_file)
{
    /* drop to having init() as parent */
    int i, lfp, pid = fork();
    char str[256] = {0};
    if (pid < 0) exit(EXIT_FAILURE);
    if (pid > 0) exit(EXIT_SUCCESS);

    if (setsid() == -1)
        exit(EXIT_FAILURE);

    for (i = getdtablesize(); i>=0; i--)
        close(i);

    i = open("/dev/null", O_RDWR);
    if (dup(i) == -1) /* stdout */
        exit(EXIT_FAILURE);
    if (dup(i) == -1) /* stderr */
        exit(EXIT_FAILURE);
    umask(027);

    lfp = open(lock_file, O_RDWR|O_CREAT, 0640);
    
    if (lfp < 0)
        exit(EXIT_FAILURE); /* can not open */

    if (lockf(lfp, F_TLOCK, 0) < 0)
        exit(EXIT_SUCCESS); /* can not lock */
    
    /* only first instance continues */
    if (ftruncate(lfp, 0) == -1)
        exit(EXIT_FAILURE);
    sprintf(str, "%d\n", getpid());
    write(lfp, str, strlen(str)); /* record pid to lockfile */

    /* SIGPIPE won't crash the server */
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
        exit(EXIT_FAILURE);
    /* Automatically reap child process */
    if (signal(SIGCHLD, SIG_IGN) == SIG_ERR)
        exit(EXIT_FAILURE);
    /* Hangup signal */
    if (signal(SIGHUP, signal_handler) == SIG_ERR)
        exit(EXIT_FAILURE);
    /* Software termination signal from kill */
    if (signal(SIGTERM, signal_handler) == SIG_ERR)
        exit(EXIT_FAILURE);

    log_log(-1, "Server daemonized with pid %d", getpid());
}

int main(int argc, char* argv[])
{
    int port, s_port;
    int www_len, cgi_len;

    /* Parse arguments */
    if (argc < 9)
    {
        fprintf(stdout, "usage: %s <HTTP port> <HTTPS port> <log file> <lock file> <www folder> <cgi folder> <ssl key> <ssl certificate>\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    port = atoi(argv[1]);
    if (port < 1 || port > 65535)
    {
        fprintf(stdout, "Invalid http port: %s\n", argv[1]);
        exit(EXIT_FAILURE);
    }
    s_port = atoi(argv[2]);
    if (s_port < 1 || s_port > 65535)
    {
        fprintf(stdout, "Invalid https port: %s\n", argv[2]);
        exit(EXIT_FAILURE);
    }
    www_len = strlen(argv[5]);
    if (www_len > MAX_PATH_LENGTH - 100) // Make more room
    {
        fprintf(stdout, "WWW folder path too long: %s\n", argv[5]);
        exit(EXIT_FAILURE);
    }
    file_io_initialize(argv[5]);
    cgi_len = strlen(argv[6]);
    if (cgi_len > MAX_PATH_LENGTH - 100) // Make more room too
    {
        fprintf(stdout, "CGI script path too long: %s\n", argv[6]);
        exit(EXIT_FAILURE);
    }
    cgi_initialize(argv[6]);
    if (!file_exist(argv[7]))
    {
        fprintf(stdout, "Private key file not exists");
        exit(EXIT_FAILURE);
    }
    if (!file_exist(argv[8]))
    {
        fprintf(stdout, "Certificate file not exists");
        exit(EXIT_FAILURE);
    }
    SSL_load_error_strings();
    SSL_library_init();
    daemonize(argv[4]);
    log_initialize(argv[3]);
    log_log(-1, "Server start");
    while (run_connections(port, s_port, argv[7], argv[8])!=STOP)
    {
        log_log(-1, "Server restart");
    }
    log_log(-1, "Server stop");
    cgi_close();
    file_io_close();
    log_close();

    return EXIT_SUCCESS;
}
