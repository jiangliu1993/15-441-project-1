#ifndef SESSION_H
#define SESSION_H

#include "request.h"
#include <sys/select.h>
#include <openssl/ssl.h>

#define DEFAULT_BUF_SIZE 8192
#define LISTEN_BACKLOG 128
#define MAX_CONNECTIONS 1000
#define TIME_OUT 10

typedef enum {RUN, RESTART, STOP} running_state;

struct connection {
    unsigned long timestamp;
    int conn_fd;
    SSL* client_context;
    int buf_size;
    request_node* current_request;
    request_queue* queue;
    struct connection* next;
    char buf[DEFAULT_BUF_SIZE];
    char close;
    char handshaking;
};

typedef struct conn_list {
    int size;
    connection* head;
    connection* tail;
} conn_list;

connection* create_http_connection(int conn_fd);
connection* create_https_connection(int conn_fd, SSL* context);
void connection_free(connection* conn);
conn_list* create_connection_list();
void connection_list_add(conn_list* list, connection* conn);
void connection_list_remove(conn_list* list, int conn_fd);
void connection_list_free(conn_list* list);

int nonblock(int sock);
int update_fdset(fd_set* readfds, fd_set* writefds, int listen_sock,
        int listen_s_sock, conn_list* list);
int start_listen_sock(int port);
running_state run_connections(int port, int s_port, char* key,
        char* cert);
void accept_http_connection(int sock, conn_list* connection_list);
void accept_https_connection(int sock, conn_list* connection_list,
        SSL_CTX* ssl_context);

int http_recv(connection* conn);
int https_recv(connection* conn);

SSL_CTX* create_ssl_context(char* key, char* cert);
SSL* accept_ssl_connection(int conn_fd, SSL_CTX* ssl_context);

void set_running_state(running_state new_state);

#endif
